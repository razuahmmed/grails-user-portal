package com.razu.up

class Users {

    Integer id
    String firstName
    String lastName
    String address
    String phone
    String email
    String dob
    String password
    String userType = GlobalConfig.USER_TYPE.USER
    String identityHash
    Long identityHashLastUpdate
    Boolean isActive = true

    Date dateCreated
    Date lastUpdated

    static constraints = {
        firstName(nullable: false)
        email(email: true, nullable: false, unique: true, blank: false)
        password(blank: false)
        identityHash(nullable: true)
        identityHashLastUpdate(nullable: true)
        lastName(nullable: true)
        address(nullable: true)
        phone(nullable: true)
        dob(nullable: true)
    }

    def beforeInsert() {
        this.password = this.password.encodeAsMD5()
    }

    def beforeUpdate() {
        this.password = this.password.encodeAsMD5()
    }
}
