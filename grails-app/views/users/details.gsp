%{--Include Main Layout--}%
<meta name="layout" content="main"/>

<div class="card">
    <div class="card-header">
        <g:message code="user" args="['Details']"/>
    </div>
    <div class="card-body">
        <g:if test="${users}">
            <table class="table">
                <tr>
                    <th class="text-right"><g:message code="first.name"/></th><td class="text-left">${users.firstName}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="last.name"/></th><td class="text-left">${users.lastName}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="email"/></th><td class="text-left">${users.email}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="address.label"/></th><td class="text-left">${users.address}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="phone.label"/></th><td class="text-left">${users.phone}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="dob.label"/></th><td class="text-left">${users.dob}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="user.type"/></th><td class="text-left">${users.userType}</td>
                </tr>
            </table>
        </g:if>
        <div class="form-action-panel">
            <g:link controller="users" action="index" class="btn btn-primary"><g:message code="cancel"/></g:link>
        </div>
    </div>
</div>