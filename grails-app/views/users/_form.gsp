<div class="form-group">
    <label><g:message code="first.name"/></label>
    <g:textField name="firstName" class="form-control" value="${users?.firstName}" placeholder="Please Enter First Name"
                 required="required"/>
    <TagLib:renderErrorMessage fieldName="firstName" model="${users}" errorMessage="please.enter.first.name"/>
</div>

<div class="form-group">
    <label><g:message code="last.name"/></label>
    <g:textField name="lastName" class="form-control" value="${users?.lastName}" placeholder="Please Enter Last Name"/>
</div>

<div class="form-group">
    <label><g:message code="address.label"/></label>
    <g:textField name="address" class="form-control" value="" placeholder="Please Enter Address"/>
</div>

<div class="form-group">
    <label><g:message code="phone.label"/></label>
    <g:textField name="phone" class="form-control" value="" placeholder="Please Enter Phone"/>
</div>

<div class="form-group">
    <label><g:message code="email.label"/></label>
    <g:field type="email" name="email" class="form-control" value="${users?.email}" placeholder="Please Enter Email"
             required="required"/>
    <TagLib:renderErrorMessage fieldName="email" model="${users}" errorMessage="Your Email Address is not Valid / Already Exist in System"/>
</div>

<div class="form-group">
    <label><g:message code="dob.label"/></label>
    <input type="date" name="dob" class="form-control">
%{--    <g:datePicker name="dob" class="form-control" value=""/>--}%
</div>

<g:if test="${!edit}">
    <div class="form-group">
        <label><g:message code="password"/></label>
        <g:passwordField name="password" class="form-control" value="${users?.password}"
                         placeholder="Please Enter Password" required="required"/>
        <TagLib:renderErrorMessage fieldName="password" model="${users}" errorMessage="Please Enter a Password."/>
    </div>
</g:if>
