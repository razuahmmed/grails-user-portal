%{--Include Main Layout--}%
<meta name="layout" content="main"/>

<div class="card">
    <div class="card-header text-center">
        <g:message code="user" args="['Profile']"/>
    </div>
    <div class="card-body">
        <g:if test="${users}">
            <table class="table">
                <tr>
                    <th class="text-right"><g:message code="first.name"/></th><td class="text-left">${users.firstName}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="last.name"/></th><td class="text-left">${users.lastName}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="address.label"/></th><td class="text-left">${users.address}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="phone.label"/></th><td class="text-left">${users.phone}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="email"/></th><td class="text-left">${users.email}</td>
                </tr>
                <tr>
                    <th class="text-right"><g:message code="dob.label"/></th><td class="text-left">${users.dob}</td>
                </tr>
            </table>
        </g:if>
    </div>
</div>