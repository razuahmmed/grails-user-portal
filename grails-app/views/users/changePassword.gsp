%{--Include Main Layout--}%
<meta name="layout" content="main"/>
<asset:stylesheet src="login.css"/>

<div class="card">
    <div class="card-header text-center">
        <g:message code="change.password"/>
    </div>

    <div class="card-body">

        <div class="col-sm-6 col-md-4 mx-auto">
            <div class="account-wall">
                <g:form controller="users" action="doChangePassword" class="form-signin">

                    <label for="previousPassword">
                        <g:message code="pre.password" default="Previous Password"/>
                    </label>
                    <g:textField name="previousPassword" class="form-control" required="required"/>

                    <label for="password">
                        <g:message code="new.password" default="New Password"/>
                    </label>
                    <g:passwordField name="password" class="form-control" required="required"/>

                    <label for="confirmPassword">
                        <g:message code="confirm.password" default="Confirm Password"/>
                    </label>
                    <g:passwordField name="confirmPassword" class="form-control" required="required"/>

                    <g:submitButton class="btn btn-lg btn-primary" name="resetPassword" value="Change Password"/>
                    <g:field type="reset" class="btn btn-lg btn-primary" name="reset" value="Clear"/>
                </g:form>
            </div>
        </div>
    </div>
</div>



