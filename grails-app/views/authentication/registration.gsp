<meta name="layout" content="public"/>

<div id="global-wrapper">
    <div id="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 mx-auto">
                    <g:img dir="images" file="grails.svg" class="profile-img"/>

                    <div class="card">
                        <div class="card-header">
                            Registration Panel
                        </div>

                        <div class="card-body">
                            <g:form controller="authentication" action="doRegistration">
                                <g:render template="/users/form"/>
                                <g:submitButton name="registration" value="Register" class="btn btn-primary"/>
                                <g:link controller="authentication" action="login" class="btn btn-primary"><g:message
                                        code="cancel"/></g:link>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>