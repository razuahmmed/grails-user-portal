<meta name="layout" content="public"/>

<div id="global-wrapper">
    <div id="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 mx-auto">
                    <g:img dir="images" file="grails.svg" class="profile-img"/>
                    <h1 class="text-center login-title">Login Panel</h1>
                    <div class="account-wall">
                        <g:form controller="authentication" action="doLogin" class="form-signin">

                            <label for="email">
                                <g:message code="email.address.label" default="Email Address"/>
                            </label>
                            <g:textField name="email" class="form-control" required="required"/>

                            <label for="password">
                                <g:message code="password.label" default="Password"/>
                            </label>
                            <g:passwordField name="password" class="form-control" required="required"/>

                            <g:submitButton class="btn btn-lg btn-primary" name="login" value="Login"/>
                            <g:field type="reset" class="btn btn-lg btn-primary" name="reset" value="Clear"/>
                            <br>
                            Are you new here? <g:link controller="authentication" action="registration">Register Now</g:link>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

