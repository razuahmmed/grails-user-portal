package com.razu.up

class AuthenticationController {

    AuthenticationService authenticationService
    UsersService usersService

    def login() {
        if (authenticationService.isAuthenticated()) {
            redirect(controller: "users", action: "profile")
        }
    }

    def doLogin() {
        if (authenticationService.doLogin(params.email, params.password)) {
            if (authenticationService.isAdminUser()) {
                redirect(controller: "users", action: "index")
            } else {
                redirect(controller: "users", action: "profile")
            }
        } else {
            flash.message = AppUtils.infoMessage("Email Address or Password not Valid.", false)
            redirect(controller: "authentication", action: "login")
        }
    }

    def logout() {
        session.invalidate()
        redirect(controller: "authentication", action: "login")
    }

    def registration() {
        [user: flash.redirectParams]
    }

    def doRegistration() {
        def response = usersService.save(params)
        if (response.isSuccess) {
            authenticationService.setUserAuthorization(response.model)
            redirect(controller: "users", action: "profile")
        } else {
            flash.redirectParams = response.model
            redirect(controller: "authentication", action: "registration")
        }
    }
}
