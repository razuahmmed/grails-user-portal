package com.razu.up

class UsersController {

    AuthenticationService authenticationService
    UsersService usersService

    def index() {
        def response = usersService.list(params)
        [usersList: response.list, total: response.count]
    }

    def notFound() {

    }

    def changePassword() {

    }

    def profile() {
        def response = usersService.getById(authenticationService.getUserId())
        if (!response) {
            redirect(controller: "users", action: "notFound")
        } else {
            [users: response]
        }
    }

    def details(Integer id) {
        def response = usersService.getById(id)
        if (!response) {
            redirect(controller: "users", action: "index")
        } else {
            [users: response]
        }
    }

    def create() {
        [users: flash.redirectParams]
    }

    def save() {
        def response = usersService.save(params)
        if (!response.isSuccess) {
            flash.redirectParams = response.model
            flash.message = AppUtils.infoMessage(g.message(code: "unable.to.save"), false)
            redirect(controller: "users", action: "create")
        } else {
            flash.message = AppUtils.infoMessage(g.message(code: "saved"))
            redirect(controller: "users", action: "index")
        }
    }

    def doChangePassword() {
        String pass = params.previousPassword
        String newPass = params.password
        String confPass = params.confirmPassword
        if (newPass.equals(confPass)) {
            def user = usersService.getById(authenticationService.getUserId())
            if (pass.encodeAsMD5().equals(user.password)) {
                def response = usersService.update(user, params)
                if (!response.isSuccess) {
                    flash.redirectParams = response.model
                    flash.message = AppUtils.infoMessage(g.message(code: "unable.to.change.password"), false)
                } else {
                    flash.message = AppUtils.infoMessage(g.message(code: "changed.password"))
                }
            } else {
                flash.message = AppUtils.infoMessage(g.message(code: "password.not.match"))
            }
        } else {
            flash.message = AppUtils.infoMessage(g.message(code: "new.conf.not.match"))
        }
        redirect(controller: "users", action: "changePassword")
    }

    def edit(Integer id) {
        if (flash.redirectParams) {
            [users: flash.redirectParams]
        } else {
            def response = usersService.getById(id)
            if (!response) {
                flash.message = AppUtils.infoMessage(g.message(code: "invalid.entity"), false)
                redirect(controller: "users", action: "index")
            } else {
                [users: response]
            }
        }
    }

    def update() {
        def response = usersService.getById(params.id)
        if (!response) {
            flash.message = AppUtils.infoMessage(g.message(code: "invalid.entity"), false)
            redirect(controller: "users", action: "index")
        } else {
            response = usersService.update(response, params)
            if (!response.isSuccess) {
                flash.redirectParams = response.model
                flash.message = AppUtils.infoMessage(g.message(code: "unable.to.update"), false)
                redirect(controller: "users", action: "edit")
            } else {
                flash.message = AppUtils.infoMessage(g.message(code: "updated"))
                redirect(controller: "users", action: "index")
            }
        }
    }

    def delete(Integer id) {
        def response = usersService.getById(id)
        if (!response) {
            flash.message = AppUtils.infoMessage(g.message(code: "invalid.entity"), false)
            redirect(controller: "users", action: "index")
        } else {
            response = usersService.delete(response)
            if (!response) {
                flash.message = AppUtils.infoMessage(g.message(code: "unable.to.delete"), false)
            } else {
                flash.message = AppUtils.infoMessage(g.message(code: "deleted"))
            }
            redirect(controller: "users", action: "index")
        }
    }
}