package com.razu.up

import grails.web.servlet.mvc.GrailsParameterMap
import grails.gorm.transactions.Transactional

@Transactional
class UsersService {

    def save(GrailsParameterMap params) {
        Users user = new Users(params)
        def response = AppUtils.saveResponse(false, user)
        if (user.validate()) {
            user.save(flush: true)
            if (!user.hasErrors()) {
                response.isSuccess = true
            }
        }
        return response
    }

    def update(Users user, GrailsParameterMap params) {
        user.properties = params
        def response = AppUtils.saveResponse(false, user)
        if (user.validate()) {
            user.save(flush: true)
            if (!user.hasErrors()) {
                response.isSuccess = true
            }
        }
        return response
    }

    def getById(Serializable id) {
        return Users.get(id)
    }

    def list(GrailsParameterMap params) {
        params.max = params.max ?: GlobalConfig.itemsPerPage()
        List<Users> userList = Users.createCriteria().list(params) {
            if (params?.colName && params?.colValue) {
                like(params.colName, "%" + params.colValue + "%")
            }
            if (!params.sort) {
                order("id", "desc")
            }
        }
        return [list: userList, count: Users.count()]
    }

    def delete(Users user) {
        try {
            user.delete(flush: true)
        } catch (Exception e) {
            println(e.getMessage())
            return false
        }
        return true
    }
}