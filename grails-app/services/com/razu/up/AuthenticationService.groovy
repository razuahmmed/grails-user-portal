package com.razu.up

class AuthenticationService {

    private static final String AUTHORIZED = "AUTHORIZED"

    def setUserAuthorization(Users user) {
        def authorization = [isLoggedIn: true, user: user]
        AppUtils.getAppSession()[AUTHORIZED] = authorization
    }

    def doLogin(String email, String password) {
        password = password.encodeAsMD5()
        Users user = Users.findByEmailAndPassword(email, password)
        if (user) {
            setUserAuthorization(user)
            return true
        }
        return false
    }

    boolean isAuthenticated() {
        def authorization = AppUtils.getAppSession()[AUTHORIZED]
        if (authorization && authorization.isLoggedIn) {
            return true
        }
        return false
    }

    def getUser() {
        def authorization = AppUtils.getAppSession()[AUTHORIZED]
        if(authorization!=null)
        return authorization?.user
    }

    def getUserId() {
        def user = getUser()
        return "${user.id}"
    }

    def getUserName() {
        def user = getUser()
        return "${user.firstName} ${user.lastName}"
    }

    def isAdminUser() {
        def user = getUser()
        if (user && user.userType == GlobalConfig.USER_TYPE.ADMIN) {
            return true
        }
        return false
    }
}